//
//  AppDelegate.h
//  p05-ferrara
//
//  Created by Dylan Ferrara on 4/15/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

