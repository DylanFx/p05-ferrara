//
//  ViewController.m
//  p05-ferrara
//
//  Created by Dylan Ferrara on 4/15/17.
//  Copyright © 2017 Dylan Ferrara. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    NSArray *buildings;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [buildings count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.text = [buildings objectAtIndex:indexPath.row];
    return cell;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    buildings = [NSArray arrayWithObjects:@"Engineering", @"Bartle", @"University Union", @"Lecture Hall",
                 @"Fine Arts", @"Science 1", @"Science 2", @"Science 3", @"Science 4", @"Science 5",
                 @"Science Library", @"Student Wing", @"Academic A", @"Academic B", @"Anderson Center",
                 @"University Union West", nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
